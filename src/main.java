import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//veureInfo ("C:\\clara\\test.txt");
		veureInfo (args[0]);
	}
	
	public static void veureInfo (String ruta) {
		File f = new File(ruta);
		if (f.isDirectory()) {
			 System.out.println("Fitxers al directori actual: ");		 String[] arxius = f.list();
			 for (int i = 0; i<arxius.length; i++){
				 System.out.println(arxius[i]);
			 }
		} else {
			System.out.println("INFORMACIÓ SOBRE EL FITXER");
			 if(f.exists()){
				 
				 SimpleDateFormat formatData = new SimpleDateFormat("MM-dd-yy HH:mm a");
				 System.out.println("Fecha de modificaccion: " +  formatData.format(f.lastModified()));

					SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yy HH:mm a");
					try {
						Date fechaInicial = dateFormat.parse(formatData.format(f.lastModified()));
						Date fechaFinal;
						
						//ACtual date time
						fechaFinal = dateFormat.parse(dateFormat.format(System.currentTimeMillis()));
						
						int dias=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/86400000);
						System.out.println("Hay "+dias+" dias de diferencia");
						if(dias >= 3) {
							System.out.println("MAS DE 3 DIAS DE LA ULTIMA MODIFICACION");
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				 
				 
				 System.out.println("Nom del fitxer : "+f.getName());
				 System.out.println("Ruta           : "+f.getPath());
				 System.out.println("Ruta absoluta  : "+f.getAbsolutePath());
				 System.out.println("Es pot escriure: "+f.canRead());
				 System.out.println("Es pot llegir  : "+f.canWrite());
				 System.out.println("Grandaria      : "+f.length());
				 System.out.println("Es un directori: "+f.isDirectory());
				 System.out.println("Es un fitxer   : "+f.isFile());
				 System.out.println("Esta ocult : " + f.isHidden());
			 }

		}
		
		

	}

}
